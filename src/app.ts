import express from 'express';
import log from '@ajar/marker';
import morgan from 'morgan';
import apiRouter from './apiRoutes.js';

const { PORT, HOST } = process.env;

const app = express();

const myMiddleware = (req: express.Request, res: express.Response, next: Function): void => {
    console.log('app middleware')
    req.body ? Object.keys(req.body).length !== 0 ? console.log("body: ", req.body) : null : null;
    req.params ? Object.keys(req.params).length !== 0 ? console.log("params: ", req.params) : null : null;
    req.query ? Object.keys(req.query).length !== 0 ? console.log("query: ", req.query) : null : null;
    next();
};

app.use( morgan('dev') );
app.use( express.json() );
app.use( myMiddleware );

app.use('/api', apiRouter);

app.get('/', (req: express.Request, res: express.Response): void => {
    res.status(200).send('Hello Express!')
})

app.get('/users', (req: express.Request, res: express.Response): void => {
    res.status(200).send('Get all Users')
})

app.get('/users/qs', (req: express.Request, res: express.Response): void => {
    res.status(200).set("Content-Type", "text/html").send(`<h3>Get user with ${req.query?.id}<h3>`)
})

app.get('/users/:id', (req: express.Request, res: express.Response): void => {
    res.status(200).set("Content-Type", "text/html").send(`<h3>Get user with ${req.params.id}</h3>`)
})

app.post('/users/add', (req: express.Request, res: express.Response): void => {
    res.status(200).json(req.body)
})

app.use('*', (req: express.Request, res: express.Response): void => {
    res.status(404).set("Content-Type", "text/html").send(`<h3>404 - page not found :/</h3>`)
})

app.listen(Number(PORT), String(HOST),  ()=> {
    log.magenta(`🌎  listening on`,`http://${HOST}:${PORT}`);
});

//------------------------------------------
//         Express Echo Server
//------------------------------------------
/* challenge instructions

     - install another middleware - morgan
        configuring app middleware like so:
        app.use( morgan('dev') );

    -  define more routing functions that use

        - req.query - access the querystring part of the request url
        - req.params - access dynamic parts of the url
        - req.body - access the request body of a POST request
        
        in each routing function you want to pass some values to the server from the client
        and echo those back in the server response

    - return api json response
    - return html markup response

    - return 404 status with a custom response to unsupported routes


*/
