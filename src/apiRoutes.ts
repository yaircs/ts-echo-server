import express from 'express';

let apiRouter = express.Router();

const routeMiddleware = (req: express.Request, res: express.Response, next: Function): void => {
    console.log('route middleware');
    next();
}

const routingFuncMiddleware = (req: express.Request, res: express.Response, next: Function): void => {
    console.log('stores/add routing func middleware');
    next();
};

apiRouter.use( routeMiddleware );

apiRouter.get('/', (req: express.Request, res: express.Response): void => {
    res.status(200).send('Hello api route!')
})

apiRouter.get('/stores', (req: express.Request, res: express.Response): void => {
    res.status(200).send('Get all stores')
})

apiRouter.post('/stores/add', routingFuncMiddleware, (req: express.Request, res: express.Response): void => {
    res.status(200).json(req.body)
})

apiRouter.get('/stores/:id', (req: express.Request, res: express.Response): void => {
    res.status(200).set("Content-Type", "text/html").send(`<h3>Get store with id of ${req.params.id}</h3>`)
})

apiRouter.use('*', (req: express.Request, res: express.Response): void => {
    res.status(404).set("Content-Type", "text/html").send(`<h3>404 - page not found :/</h3>`)
})
export default apiRouter;